/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zac
 */
public class HasherTest {
    
    public HasherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getHash method, of class Hasher.
     */
    
    /*
    		[0]	193	byte
		[1]	239	byte
		[2]	61	byte
		[3]	167	byte
		[4]	176	byte
		[5]	18	byte
		[6]	203	byte
		[7]	59	byte
		[8]	119	byte
		[9]	7	byte
		[10]	25	byte
		[11]	11	byte
		[12]	107	byte
		[13]	221	byte
		[14]	246	byte
		[15]	184	byte
		[16]	211	byte
		[17]	16	byte
		[18]	141	byte
		[19]	183	byte    
    */
    
    @Test
    public void testGetHash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        System.out.println("getHash");
        String input = "This is a test message";
        int[] expResult = new int[] {193, 239, 61, 167, 176, 18, 203, 59, 119, 7, 25, 11, 107, 221, 246, 184, 211, 16, 141, 183};
        int[] result = Hasher.getHash(input);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testGetSignature() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException, SignatureException, Exception {
        String toSign = "This is a test message";
        int[] hash = Hasher.getHash(toSign);
        Signature signer = Signature.getInstance("SHA1withRSA");
        byte[] bytehash = Ext.toByteArray(hash);
        signer.initSign(EncryptionManager.getPrivateKey());
        byte[] toSignBytes = toSign.getBytes("UTF-8");
        signer.update(toSignBytes);
        byte[] signature = signer.sign();
    }
}
