/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.Reply;
import Networking.Conversation.Conversation;
import Networking.Conversation.ConversationFactory;
import Networking.Conversation.LoginConversation;
import dsoak.MyBalloonStore;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zac
 */
public class DispatcherTest {
    
    public DispatcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MyBalloonStore player = MyBalloonStore.getTestPlayer();
        MyBalloonStore.ActivePlayer = player;
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Dispatch method, of class Dispatcher.
     */
    @Test
    public void testAliveConversation() throws UnknownHostException {
        assert(MyBalloonStore.ActivePlayer.getPlayerState() == MyBalloonStore.PlayerState.LoggedOut);
        System.out.println("Dispatch");
        Envelope envelope;
        IPAddress addr = new IPAddress(InetAddress.getLocalHost(), 60000);
        envelope = new Envelope("{\"__type\":\"AliveRequest:#Messages.RequestMessages\",\"ConvId\":{\"Pid\":1,\"Seq\":4510},\"MsgId\":{\"Pid\":1,\"Seq\":4510}}", addr);
        for(int i=0; i<100; i++) {
            Dispatcher.Dispatch(envelope);
        }
    }
    
    /*@Test
    public void testPlayerStatesAndConversationResults() throws InterruptedException {
        int waitTime = 5000;
        assert(Player.ActivePlayer.getPlayerState() == Player.PlayerState.LoggedOut);
        Player.ActivePlayer.login();
        Thread.sleep(waitTime);
        assert(Player.ActivePlayer.getPlayerState() == Player.PlayerState.LoggedIn);
        Player.ActivePlayer.getGameInfos();
        Thread.sleep(waitTime);
        assert(Player.ActivePlayer.getPlayerState() == Player.PlayerState.HasGameList);
        Player.ActivePlayer.joinGame();
        Thread.sleep(waitTime);
        assert(Player.ActivePlayer.getPlayerState() == Player.PlayerState.Playing);
    }*/
}
