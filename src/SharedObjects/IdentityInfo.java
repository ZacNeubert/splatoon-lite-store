package SharedObjects;

//[DataContract]

import java.util.logging.Level;
import java.util.logging.Logger;

public class IdentityInfo
{
    //[DataMember]
    public String ANumber;
    //[DataMember]
    public String FirstName;
    //[DataMember]
    public String LastName;
    //[DataMember]
    public String Alias;

    public IdentityInfo(String aNumber, String firstName, String lastName, String alias) {
        ANumber = aNumber;
        FirstName = firstName;
        LastName = lastName;
        Alias = alias;
    }
    
    public IdentityInfo Clone()
    {
        try {
            return (IdentityInfo) this.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(IdentityInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}