package SharedObjects;

//[DataContract]

import Extensions.Ext;
import Networking.EncryptionManager;
import Networking.Hasher;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public abstract class SharedResource
{
    //[DataMember]
    public int Id;
    
    public int SignedBy;

    //[DataMember]
    public int[] DigitalSignature;

    public boolean ValidateSignature()
    {
        // TODO: Add RSA parameter(s) and implement
        return true;
    }

    @Override
    public String toString()
    {
        return Id+"";
    }

    public void sign() {
        this.SignedBy = 6; //probably
        byte[] data = new byte[4];
        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.putInt(Id);
        int[] unsignedData = Ext.getUnsignedInts(data);
        byte[]unsignedDataBytes = Ext.toByteArray(unsignedData);
        
        Signature signer;
        try {
            byte[] hash = Ext.toByteArray(Hasher.getHash(data));
            signer = Signature.getInstance("SHA1withRSA");
            signer.initSign(EncryptionManager.getPrivateKey());
            signer.update(hash);
            byte[] signature = signer.sign();
            DigitalSignature = Ext.getUnsignedInts(signature);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Balloon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Balloon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Balloon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Balloon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Balloon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean verify() {
        try {
            byte[] data = new byte[4];
            ByteBuffer buffer = ByteBuffer.wrap(data);
            buffer.putInt(Id);
            byte[] myHash = Ext.toByteArray(Hasher.getHash(data));
            byte[] dsbytes = Ext.toByteArray(DigitalSignature);
            Cipher rsaCipher = Cipher.getInstance("RSA");
            rsaCipher.init(Cipher.DECRYPT_MODE, EncryptionManager.pennyBankPublicKey);
            byte[] sessionCipher = rsaCipher.doFinal(dsbytes);
            
            for(int i=0; i<sessionCipher.length; i++) {
                if(myHash[i] != sessionCipher[i]) {
                    return false;
                }
            }
            return true;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SharedResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(SharedResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(SharedResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(SharedResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(SharedResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    //#endregion
}