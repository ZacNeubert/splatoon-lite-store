package SharedObjects;

//[DataContract]

import Extensions.Ext;
import Networking.EncryptionManager;
import dsoak.MyBalloonStore;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;

public class Balloon extends SharedResource
{
    //,"Balloon":{"DigitalSignature":null,"Id":89,"IsFilled":false}
    
    //[DataMember]
    public boolean IsFilled;

    public String ToString()
    {
        return String.format("Balloon %d, IsFilled=%d", Id, IsFilled);
    }
}
