/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsoak;

import Extensions.BitConverter;
import Extensions.Ext;
import Extensions.L;
import GUI.PlayerGUI;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.GameListRequest;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.RequestMessages.LoginRequest;
import Networking.Conversation.Conversation;
import Networking.Conversation.ConversationFactory;
import Networking.Conversation.GameListConversation;
import Networking.Conversation.JoinGameConversation;
import Networking.Conversation.LeaveGameConversation;
import Networking.Conversation.LoginConversation;
import Networking.Conversation.LogoutConversation;
import Networking.Conversation.NextIdConversation;
import Networking.Conversation.RaiseUmbrellaConversation;
import Networking.Envelope;
import Networking.IPAddress;
import Networking.JsonIPEndPoint;
import Networking.UdpClient;
import SharedObjects.Balloon;
import SharedObjects.GameInfo;
import SharedObjects.GameProcessData;
import SharedObjects.IdentityInfo;
import SharedObjects.Penny;
import SharedObjects.ProcessInfo;
import SharedObjects.ProcessInfo.ProcessType;
import SharedObjects.ProcessInfo.StatusCode;
import SharedObjects.Umbrella;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacneubert
 */
public class MyBalloonStore extends Thread {
    /*
    Needs to do three things:
        Respond to alive request
        Play the game (active stuff)
        Display Status
    */
    
    public Queue<String> guiMessages = new ConcurrentLinkedQueue<>();
    
    public IdentityInfo identityInfo;
    public IPAddress LocalIP;
    public IPAddress RegistryIP;
    public String ProcessLabel;
    static final int ProcessType = 4;
    
    public ProcessInfo PlayerInfo;
    public ProcessInfo[] OtherPlayerInfos;
    public int GameManagerId;
    
    public JsonIPEndPoint ProxyJsonEndPoint;
    public JsonIPEndPoint PennyBankJsonEndPoint;
    
    public Boolean LoggedIn = false;
    
    public List<GameInfo> GameInfos = null;
    
    public Boolean JoinedGame = false;
    public int LifePoints = 0;
    public GameInfo CurrentGameInfo;

    public int PurchaseAttempts=0;    
    
    public List<Conversation> Conversations;
    private Object conversationlock = new Object();
    public void addConversation(Conversation c, String hint) {
        if(c==null) {
            int i=0;
            i++;
        }
        synchronized(conversationlock) {
            Conversations.add(c);
        }
    }
    
    public int BalloonSupplierId;
    public int BalloonFillerId;
    
    UdpClient udpClient;
    public int ActiveGameId = -1;
    
    public List<Queue<Penny>> Wallets = null;
    public final int TotalWallets = 5;
    private int currentWallet = 0;
    public int balloonTargetProcess;
    public void initializeWallets() {
        if(Wallets == null) {
            Wallets = new ArrayList<Queue<Penny>>();
            for(int i=0; i<TotalWallets; i++) {
                Wallets.add(new ConcurrentLinkedQueue<Penny>());
            }
        }
    }
    public void addPenny(String penny, int CreatorId) {
        initializeWallets();
        Penny p = (Penny) Ext.decodeJson(penny, Penny.class);
        p.DigitalSignature = Ext.getUnsignedInts(p.DigitalSignature);
        addPenny(p);
    }
    public void addPenny(Penny p) {
        initializeWallets();
        int index = p.Id;
        while(index >= TotalWallets) {
            index = index - TotalWallets;
        }
        Wallets.get(index).add(p);
    }
    Random random = new Random();
    public Penny getPenny() {
        initializeWallets();
        int i = currentWallet % TotalWallets;
        currentWallet++;
        Penny p = Wallets.get(i).poll();
        if(p==null) {
            p = Wallets.get(random.nextInt(TotalWallets)).poll();
        }
        return p;
    }
    public Penny absoluteGetPenny() {
        Penny p = null;
        while(p==null) {
            p=getPenny();
            Ext.sleep(100L);
        }
        return p;
    }
    public int WalletSize() {
        initializeWallets();
        int size=0;
        for(Queue<Penny> wallet : Wallets) {
            size+=wallet.size();
        }
        return size;
    }
    
    private Queue<Balloon> BalloonBox =  new ConcurrentLinkedQueue<>();
    public void addBalloon(String balloon) {
        Balloon p = (Balloon) Ext.decodeJson(balloon, Balloon.class);
        BalloonBox.add(p);
    }
    public void addBalloon(Balloon balloon) {
        BalloonBox.add(balloon);
    }
    public Balloon getBalloon() {
        Balloon b = BalloonBox.poll();
        return b;
    }
    public Balloon absoluteGetBalloon() {
        Balloon b = null;
        while(b == null) {
            if(BalloonBoxSize() == 0) return null;
            b = BalloonBox.poll();
        }
        return b;
    }
    public int BalloonBoxSize() {
        return BalloonBox.size();
    }
    public static MyBalloonStore ActivePlayer = null;
    
    public int passedGameId;
    public int passedGamemanagerId;
    public int passedBalloonCount;
    public int passedStoreIndex;
    public MyBalloonStore(IdentityInfo ii, IPAddress localIP, IPAddress destIP, String processLabel, int gameid, int gamemanagerid, int ballooncount, int storeindex) throws SocketException {
        passedGameId = gameid;
        passedGamemanagerId = gamemanagerid;
        passedBalloonCount = ballooncount;
        passedStoreIndex = storeindex;
        
        guiMessages = new ConcurrentLinkedQueue<String>();
        ConversationFactory.initialize(this);
        Conversations = Collections.synchronizedList(new ArrayList());
                
        identityInfo = ii;
        LocalIP = localIP;
        RegistryIP = destIP;
        ProcessLabel = processLabel;
        
        ActivePlayer = this;
        
        try {
            udpClient = new UdpClient(0);
            udpClient.start();
            
            //GUI.setVisible(true);
        }
        catch(InterruptedException ie) {
            L.l.log(Level.SEVERE, ie.getMessage());
        }
    }
    
    public static MyBalloonStore getTestPlayer() { //FOR TEST CASES ONLY
        return null;
        /*IdentityInfo ii = new IdentityInfo(
            "ATESTCASE",
            "TestFirstName",
            "TestLastName",
            "6" //probably
        );
        
        
        IPAddress destIP;
        IPAddress localIP;
        try {
            destIP = new IPAddress(InetAddress.getLocalHost(), 12000);
            localIP = new IPAddress(InetAddress.getLocalHost(), 0);
            String pLabel = "Label";
            
            MyBalloonStore p = new MyBalloonStore(ii, localIP, destIP, pLabel);
            for(int i=0; i<10; i++) {
                p.addAmmo(new Balloon());
                p.addBalloon(new Balloon());
            }
            return p;
        } catch (UnknownHostException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
        catch (SocketException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
        return null;*/
    }
    
    public void login() {
        try {
            Conversation loginConv = ConversationFactory.create(LoginConversation.key);
            addConversation(loginConv, "Login");
            loginConv.start();
        }
        catch (Exception e) {
            e.printStackTrace();
            L.Warn(e.getLocalizedMessage());
            L.Warn(e.getMessage());
            L.Warn(e.toString());
        }
    }
    
    public void getGameInfos() {
        try {
            Conversation gameListConv = ConversationFactory.create(GameListConversation.key);
            addConversation(gameListConv, "GameList");
            gameListConv.start();
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void joinGame() {
        try {
            Conversation joinGameConv = ConversationFactory.create(JoinGameConversation.key);
            addConversation(joinGameConv, "JoinGame");
            joinGameConv.start();
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getIds() {
        try {
            Conversation nextIdConv = ConversationFactory.create(NextIdConversation.key);
            addConversation(nextIdConv, "NextId");
            nextIdConv.start();
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean send(String message) {
        return send(message, RegistryIP);
    }
    
    public boolean send(String message, IPAddress ip) {
        byte[] bytes = message.getBytes();
        
        L.l.log(Level.INFO, "Sending to " + ip.Host.toString() + ":" + ip.Port + "\n\t" + message);
        
        try {
            Envelope envelope = new Envelope(message, ip);
            udpClient.SendEnvelope(envelope);
        }
        catch(IOException ioe) {
            L.l.log(Level.SEVERE, ioe.getMessage());
            return false;
        }
        catch(InterruptedException ie) {
            L.l.log(Level.SEVERE, ie.getMessage());
            return false;
        }
        
        return true;
    }

    public synchronized void decrementLifePoints() {
        LifePoints--;
    }

    public List<GameProcessData> balloonStores = new ArrayList<>();
    public List<GameProcessData> waterSources = new ArrayList<>();
    public List<GameProcessData> enemies = new ArrayList<>();
    public List<GameProcessData> umbrellaSuppliers = new ArrayList<>();
    //public enum ProcessType { Unknown = 0,Registry, GameManager, Player, BalloonStore, WaterServer, UmbrellaSupplier, PennyBank, Proxy }                                     ;
    public void initializeProcesses(GameProcessData[] gpds) {
        balloonStores = new ArrayList<>();
        waterSources = new ArrayList<>();
        enemies = new ArrayList<>();
        for(GameProcessData gpd : gpds) {
            if(gpd.Type == 4) balloonStores.add(gpd);
            else if(gpd.Type == 5) waterSources.add(gpd);
            else if(gpd.Type == 3 && gpd.ProcessId != this.PlayerInfo.ProcessId) enemies.add(gpd);
            else if(gpd.Type == 6) umbrellaSuppliers.add(gpd);
        }
    }
    public int getBalloonStore() {
        if(balloonStores.size() > 0) return balloonStores.get(0).ProcessId;
        return -1;
    }
    public int getWaterSource() {
        if(waterSources.size() > 0) return waterSources.get(0).ProcessId;
        return -1;
    }
    public int getEnemy() {
        if(enemies.size() > 0) return enemies.get(0).ProcessId;
        return -1;
    }
    
    private int balloonsThrown = 0;
    private Object throwLock = new Object();
    public void incrementThrown() {
        synchronized(throwLock) {
            balloonsThrown++;
        }
    }
    public int thrownCount() {
        synchronized(throwLock) {
            return balloonsThrown;
        }
    }
    
    public void resetThrown() {
        synchronized(throwLock) {
            balloonsThrown=0;
        }
    }

    public void dumpBalloonStore() {
        if(balloonStores.size() > 1) balloonStores.remove(0);
    }

    private Umbrella umbrella;
    public void setUmbrella(Umbrella Umbrella) {
        umbrella = Umbrella;
    }
    public Umbrella getUmbrella() {
        return umbrella;
    }

    private int minimumBid = 1;
    public void setMinimumBid(int MinimumBid) {
        minimumBid = MinimumBid;
    }
    public int getMinimumBid() {
        return minimumBid;
    }

    int nextId = -1;
    public void setNextId(int NextId) {
        nextId = NextId;
    }

    int idCount = -1;
    public void setIdCount(int NumberOfIds) {
        idCount = NumberOfIds;
    }

    private Map usedPennies = new ConcurrentHashMap<Integer, Boolean>();
    public void addUsedPennies(int[] PennyIds) {
        for(int p : PennyIds) {
            usedPennies.putIfAbsent(p, true);
        }
    }
    public boolean pennyIsUsed(int id) {
        return usedPennies.containsKey(id);
    }
    
    public enum PlayerState {
        LoggedOut,LoggingIn,LoggedIn,GettingGameList,HasGameList,JoiningGame,WaitingForGameStart,Playing,LoggingOut,Exit,GettingIds,HasIds,LeavingGame,LeftGame, OutOfBalloons
    }
    
    private PlayerState playerState = PlayerState.LoggedOut;
    private Object stateLock = new Object();
    public synchronized PlayerState getPlayerState() {
        synchronized(stateLock) {
            return playerState;
        }
    }
    public void setPlayerState(PlayerState state) {
        synchronized(stateLock) {
            playerState = state;
        }
    }
    
    @Override
    public void run() {
        while(true) {
            PlayerState playerState = getPlayerState();
            if(playerState == PlayerState.LoggedOut) {
                login();
            }
            if(playerState == PlayerState.LoggedIn) {
                this.setPlayerState(MyBalloonStore.PlayerState.GettingGameList);
                getGameInfos();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if(playerState == PlayerState.HasGameList) {
                getIds();
            }
            if(playerState == PlayerState.HasIds) {
                joinGame();
            }
            if(BalloonBoxSize() == 0 && (playerState == PlayerState.WaitingForGameStart || playerState == PlayerState.Playing)) {
                //this.setPlayerState(PlayerState.OutOfBalloons);
                try {
                    Conversation leaveGameConversation = ConversationFactory.create(LeaveGameConversation.key);
                    MyBalloonStore.ActivePlayer.addConversation(leaveGameConversation, "Leave Game");
                    leaveGameConversation.start();
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //if(playerState == PlayerState.OutOfBalloons) {

            //}
            if(playerState == PlayerState.LeftGame) {
                try {
                    Conversation logoutConversation = ConversationFactory.create(LogoutConversation.key);
                    MyBalloonStore.ActivePlayer.addConversation(logoutConversation, "logout");
                    logoutConversation.start();
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(playerState == PlayerState.Exit) {
                break;
            }
            
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyBalloonStore.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
