package JavaMessage.RequestMessages;

import SharedObjects.Penny;
import dsoak.MyBalloonStore;

public class PennyValidation extends Request
{
    
    String __type = "PennyValidation:#Messages.RequestMessages";
    //[DataMember]
    public Penny[] Pennies;

    //[DataMember]
    public boolean MarkAsUsedIfValid;
    
    public static boolean isRouted() {
        return false;
    }
    
    public PennyValidation(MyBalloonStore player) {
        MarkAsUsedIfValid = true;
    }
}