package JavaMessage.RequestMessages;

import SharedObjects.GameInfo;
import SharedObjects.ProcessInfo;
import dsoak.MyBalloonStore;

public class JoinGameRequest extends Request
{
    String __type = "JoinGameRequest:#Messages.RequestMessages";
    public int GameId;
    public ProcessInfo Process;
    
    public static boolean isRouted() {
        return true;
    }
    
    public JoinGameRequest(MyBalloonStore balloonstore) {
        GameInfo gi = balloonstore.GameInfos.get(0);
        for(GameInfo gamei : balloonstore.GameInfos) {
            if(gamei.GameId == balloonstore.passedGameId) {
                gi = gamei;
                break;
            }
        }
        GameId = gi.GameId;
        Process = balloonstore.PlayerInfo;
    }
}
