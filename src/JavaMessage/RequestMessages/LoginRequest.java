package JavaMessage.RequestMessages;

import Extensions.Ext;
import Networking.EncryptionManager;
import SharedObjects.IdentityInfo;
import SharedObjects.IntPublicKey;
import SharedObjects.ProcessInfo;
import SharedObjects.PublicKey;
import dsoak.MyBalloonStore;

public class LoginRequest extends Request
{
    public String __type = "LoginRequest:#Messages.RequestMessages";
    public int ProcessType = 4;
    public String ProcessLabel;
    public IdentityInfo Identity;
    public IntPublicKey PublicKey;
    
    public LoginRequest(MyBalloonStore player) {
        ProcessLabel = player.ProcessLabel;
        Identity = player.identityInfo;
        try {
            PublicKey = EncryptionManager.getPublicKey();
            PublicKey.Modulus = Ext.getUnsignedInts(PublicKey.Modulus);
        }
        catch (Exception e) {
            int i=0;
            i++;
            //Log.e(e.toString());
        }
    }
}
