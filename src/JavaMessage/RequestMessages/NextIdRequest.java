package JavaMessage.RequestMessages;
//[DataContract]

import dsoak.MyBalloonStore;

public class NextIdRequest extends Request
{
    String __type="NextIdRequest:#Messages.RequestMessages";
    
    public int NumberOfIds;
    
    public static boolean isRouted() {
        return false;
    }
    
    public NextIdRequest(MyBalloonStore player) {
        NumberOfIds = player.passedBalloonCount;
    }
}