package JavaMessage.RequestMessages;

import SharedObjects.Umbrella;

public class RaiseUmbrellaRequest extends Request
{
    public static boolean isRouted() {
        return true;
    }
    
    //[DataMember]
    public Umbrella Umbrella;
}
