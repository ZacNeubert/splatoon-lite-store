package JavaMessage.RequestMessages;

import dsoak.MyBalloonStore;

public class GameListRequest extends Request
{
    public String __type = "GameListRequest:#Messages.RequestMessages";
    public int StatusFilter = 4;
    
    public GameListRequest(MyBalloonStore player) {
        super();
    }
}