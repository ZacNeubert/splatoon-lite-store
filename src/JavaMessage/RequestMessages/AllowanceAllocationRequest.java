package JavaMessage.RequestMessages;

import JavaMessage.RequestMessages.Request;

public class AllowanceAllocationRequest extends Request
{
    //[DataMember]
    public int ToProcessId;

    //[DataMember]
    public int Amount;
}