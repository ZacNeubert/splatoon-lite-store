package JavaMessage.RequestMessages;

//[DataContract]
public class DeadProcessNotification extends Request
{
    //[DataMember]
    public int ProcessId;
            
    public static boolean isRouted() {
        return false;
    }
}
