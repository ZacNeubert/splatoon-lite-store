package JavaMessage.RequestMessages;

//[DataContract]

import SharedObjects.Penny;
import dsoak.MyBalloonStore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BuyBalloonRequest extends Request
{
    String __type="BuyBalloonRequest:#Messages.RequestMessages";
    
    //[DataMember]
    public Penny Penny = null;
    
    public static boolean isRouted() {
        return true;
    }
    
    public BuyBalloonRequest(MyBalloonStore player) {
        while(this.Penny == null) {
            try {
                Thread.sleep(10*player.BalloonBoxSize());
            } catch (InterruptedException ex) {
                Logger.getLogger(BuyBalloonRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.Penny = player.getPenny();
        }
    }
}