/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaMessage;

import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.LoginRequest;
import JavaMessage.RequestMessages.Request;
import SharedObjects.Balloon;
import com.google.gson.Gson;
import java.lang.ProcessBuilder.Redirect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacneubert
 */
public class Message
{    
    private static List<Class> serializableTypes = new ArrayList<Class>()
    {{
        add(AliveRequest.class);
        //add(GameListRequest.class);
        //add(GameListReply.class);
        //add(JoinGameReply.class);
        //add(JoinGameRequest.class);
        add(LoginReply.class);
        add(LoginRequest.class);
        //add(LogoutRequest.class);
        //add(NextIdRequest.class);
        //add(NextIdReply.class);
        //add(RegisterGameReply.class);
        //add(RegisterGameRequest.class);
        add(Reply.class);
        add(Request.class);
        //add(ValidateProcessRequest.class);
    }};

    public MessageNumber MsgId;
    public MessageNumber ConvId;
    
    public void InitMessageAndConversationNumbers() {
        SetMessageAndConversationNumbers(MessageNumber.Create());
    }
    
    public void SetMessageAndConversationNumbers(MessageNumber id) {
        SetMessageAndConversationNumbers(id, id.clone());
    }
    
    public void SetMessageAndConversationNumbers(MessageNumber id, MessageNumber convId) {
        MsgId = id;
        ConvId = convId;
    }
    
    public static boolean isRouted() {
        return false;
    }
    
    public Message getClone() {
        try {
            return (Message) this.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Message()
    {

    }
    
    public Balloon Balloon;
}
