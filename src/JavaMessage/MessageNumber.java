package JavaMessage;
public class MessageNumber
{
    //#region Private Properties
    private static int _nextSeqNumber;                     // Start with message #1
    private static final Object MyLock = new Object();
    //#endregion

    //#region Public Properties
    public static int LocalProcessId;            // Local process Id -- set once when the
                                                                // process joins the distributed application
    //[DataMember]
    public int Pid;
    //[DataMember]
    public int Seq;

    @Override
    public MessageNumber clone() {
        MessageNumber cl = new MessageNumber();
        cl.Pid = this.Pid;
        cl.Seq = this.Seq;
        
        return cl;
    }
    
    //#endregion

    //#region Constructors and Factories
    /// <summary>
    /// Factory method creates and new, unique message number.
    /// </summary>
    /// <returns>A new message number</returns>
    public static MessageNumber Create()
    {
        MessageNumber result = new MessageNumber();
        result.Pid = LocalProcessId;
        result.Seq = GetNextSeqNumber();
        return result;
    }
    //#endregion

    //#region Public Methods for Testing
    public static void ResetSeqNumber() { _nextSeqNumber = 0; }
    public static void SetSeqNumber(int newValue) { 
        _nextSeqNumber = (0 > newValue) ? 0 : newValue;
    }
    //#endregion

    //#region Overridden public methods of Object
    @Override
    public String toString()
    {
        return Pid + "." + Seq;
    }
    //#endregion

    //#region Private Methods
    private static int GetNextSeqNumber()
    {
        synchronized (MyLock)
        {
            if (_nextSeqNumber == Integer.MAX_VALUE)
                _nextSeqNumber = 0;
            ++_nextSeqNumber;
        }
        return _nextSeqNumber;
    }
    //#endregion

    //#region Comparison Methods and Operators
    @Override
    public boolean equals(Object obj)
    {
        return Compare(this, (MessageNumber) obj) == 0;
    }

    public static int Compare(MessageNumber a, MessageNumber b)
    {
        int result = 0;

        if (!(a == b))
        {
            if (((Object) a == null) && ((Object) b != null))
                result = -1;                             
            else if (((Object) a != null) && ((Object) b == null))
                result = 1;                             
            else
            {
                if (a.Pid < b.Pid)
                    result = -1;
                else if (a.Pid > b.Pid)
                    result = 1;
                else if (a.Seq < b.Seq)
                    result = -1;
                else if (a.Seq > b.Seq)
                    result = 1;
            }
        }
        return result;
    }

    //#endregion
}