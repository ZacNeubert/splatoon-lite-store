package JavaMessage.ReplyMessages;

import dsoak.MyBalloonStore;

public class JoinGameReply extends Reply
{
    public int GameId;
    public int InitialLifePoints;
    
    private JoinGameReply(MyBalloonStore player) {
        super(player);
    }
}
