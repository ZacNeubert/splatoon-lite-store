package JavaMessage.ReplyMessages;

import SharedObjects.GameInfo;
import dsoak.MyBalloonStore;

public class GameListReply extends Reply
{
//    public String __type;
    public GameInfo[] GameInfo;
    
    public GameListReply(MyBalloonStore player) {
        super(player);
    }
}