package JavaMessage.ReplyMessages;

//[DataContract]

import dsoak.MyBalloonStore;

public class RegisterGameReply extends Reply
{
    public RegisterGameReply(MyBalloonStore player) {
        super(player);
    }
}