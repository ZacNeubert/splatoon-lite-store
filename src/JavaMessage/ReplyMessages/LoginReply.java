package JavaMessage.ReplyMessages;

import Networking.JsonIPEndPoint;
import SharedObjects.ProcessInfo;
import SharedObjects.PublicKey;
import dsoak.MyBalloonStore;

public class LoginReply extends Reply
{
    public ProcessInfo ProcessInfo;
    public JsonIPEndPoint PennyBankEndPoint;
    public JsonIPEndPoint ProxyEndPoint;
    public PublicKey PennyBankPublicKey;
    public String PublicKey;
    
    public LoginReply(MyBalloonStore p) {
        super(p);
    }
}
