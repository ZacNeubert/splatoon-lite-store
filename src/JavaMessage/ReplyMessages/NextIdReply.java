package JavaMessage.ReplyMessages;
//[DataContract]

import dsoak.MyBalloonStore;

public class NextIdReply extends Reply
{
    //[DataMember]
    public int NextId;
    
    public int NumberOfIds;
    
    public NextIdReply(MyBalloonStore player) {
        super(player);
    }
}
