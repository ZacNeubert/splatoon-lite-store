package JavaMessage.ReplyMessages;
//[DataContract]

import SharedObjects.PublicKey;
import dsoak.MyBalloonStore;

public class PublicKeyReply extends Reply
{
    //[DataMember]
    public int ProcessId;
    //[DataMember]
    public PublicKey Key;
    
        
    public PublicKeyReply(MyBalloonStore player) {
        super(player);
    }
}