package JavaMessage.ReplyMessages;

import JavaMessage.Message;
import dsoak.MyBalloonStore;

public class Reply extends Message
{
    public String __type="Reply:#Messages.ReplyMessages";
    public Boolean Success;
    public String Note;
    
    public Reply(MyBalloonStore player) {
        Success = true;
        Note = "";
    }
}