/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaMessage.ReplyMessages;

import JavaMessage.Message;
import dsoak.MyBalloonStore;

/**
 *
 * @author Zac
 */
public class RoutedReply extends Message
{
    public String __type="Reply:#Messages.ReplyMessages";
    public Boolean Success;
    public String Note;
    
    public RoutedReply(MyBalloonStore player) {
        Success = true;
        Note = "";
    }
    
    public static boolean isRouted() {
        return true;
    }
}
