package JavaMessage.ReplyMessages;

import SharedObjects.Umbrella;
import dsoak.MyBalloonStore;


//[DataContract]
public class BidAck extends Reply
{
//    [DataMember]
    public boolean Won;
//    [DataMember]
    public Umbrella Umbrella;

    public BidAck(MyBalloonStore player) {
        super(player);
    }
}