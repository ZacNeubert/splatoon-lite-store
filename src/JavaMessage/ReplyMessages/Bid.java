package JavaMessage.ReplyMessages;
import JavaMessage.Message;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.Routing;
import SharedObjects.Penny;
import dsoak.MyBalloonStore;

    //[DataContract]
public class Bid extends Message
{
    public String __type="Bid:#Messages.ReplyMessages";
    public Boolean Success;
    public String Note;
    //[DataMember]
    public Penny[] Pennies;
    
    public static boolean isRouted() {
        return true;
    }
    
    public Bid(MyBalloonStore player) {        
        //__type="Bid:#Messages.ReplyMessages";
        
        Success=true;
        Note="";
        
        int size = player.getMinimumBid()+1;
        //int size=1;
        Pennies = new Penny[size];
        for(int i=0; i<size; i++) {
            Pennies[i] = player.absoluteGetPenny();
        }
    }
}
