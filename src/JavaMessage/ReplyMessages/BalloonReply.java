package JavaMessage.ReplyMessages;

//[DataContract]

import SharedObjects.Balloon;
import dsoak.MyBalloonStore;

public class BalloonReply extends RoutedReply
{
    //[DataMember]
    //public Balloon Balloon;
    
    
    
    public BalloonReply(MyBalloonStore player) {
        super(player);
        
        __type = "BalloonReply:#Messages.ReplyMessages";
        
        Balloon = player.getBalloon();
        if(Balloon == null) {
            this.Success = false;
            this.Note = "No balloons left in inventory";
        }
        else {
            this.Success = true;
            this.Note = "";
        }
    }
}
