/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

/**
 *
 * @author zacneubert
 */
import Extensions.Ext;
import Extensions.L;
import dsoak.MyBalloonStore;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpClient extends Thread {

	public int Port;
	public DatagramSocket socket;
	public DatagramSocket sendSocket;
	public InetSocketAddress addr;	
	private boolean initialized;
	private DatagramPacket packet;
	public IAsyncResult ar;
	
	public IPAddress clientEP;
        
        public static UdpClient udpClient;
	
	public UdpClient(int port) throws SocketException, InterruptedException {
		Port = port;
		addr = new InetSocketAddress(port);
		initialized = false;
                
                try {
                    socket = new DatagramSocket(Port, InetAddress.getByName("0.0.0.0"));
                    socket.setSoTimeout(10000);
                }
                catch (UnknownHostException ex) {
                    L.l.log(Level.SEVERE, null, ex);
                }
                
		L.l.info("Constructed UdpClient.");
                
                udpClient = this;
	}
	
	private void SendTo(byte[] message, IPAddress destination) throws IOException, InterruptedException {
            L.l.info("Sending message to " + destination.Host.toString() + ":" + destination.Port);

            DatagramPacket packet = new DatagramPacket(message, message.length, destination.Host, destination.Port);
            socket.send(packet);

	}
        
        public void SendEnvelope(Envelope envelope) throws IOException, InterruptedException {
            byte[] message = envelope.Message.getBytes();
            try {
                String prettyMessage = Ext.prettyJson(envelope.Message);
                String[] lines = prettyMessage.split("\n");
                if(prettyMessage.contains("Balloon")) {
                    MyBalloonStore.ActivePlayer.guiMessages.add(prettyMessage + "\nSent to: " + MyBalloonStore.ActivePlayer.RegistryIP);
                }
            }
            catch(Exception e) {//do NOT fail from printing wrong {
                int i=0;
                i++;
            }
            SendTo(message, envelope.Endpoint);
        }

	public void run() {
            while(true) {
		try {
                    byte[] receiveData = new byte[1024*10];
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    //L.l.info("Listening on " + socket.getLocalAddress() + ":" + socket.getLocalPort());
                    socket.receive(receivePacket);
                    String message = new String(receiveData);
                    
                    try {
                        if(!message.trim().contains("AliveRequest")) {
                            String prettyMessage = Ext.prettyJson(message.trim());
                            if(message.contains("BalloonReply")) {
                                MyBalloonStore.ActivePlayer.guiMessages.add(Ext.prettyJson(message.trim()));
                            }
                            String[] lines = prettyMessage.split("\n");
                            for(String line : lines) {
                                if(line.contains("__type")) {
                                    MyBalloonStore.ActivePlayer.guiMessages.add("Received \n" + line);
                                }
                            }
                        }
                        else {
                            MyBalloonStore.ActivePlayer.guiMessages.add("Received AliveRequest");
                            
                        }
                    }
                    catch(Exception e) {
                        int i=0;
                        i++;
                    }
                    IPAddress ipaddress = new IPAddress(receivePacket.getAddress(), receivePacket.getPort());
                    Envelope envelope = new Envelope(message, ipaddress);
                    //Dispatcher.Dispatch(envelope);
                    CourierThread courier = new CourierThread(envelope);
                    courier.start();
                    //MessageHandler.Handle(receiveData);
                    L.l.info("Got Message: " + new String(receiveData));
		} 
                catch(SocketTimeoutException ste) {
                    // fine by me
                }
                catch (Exception e) {
			L.l.log(Level.SEVERE, e.toString());
		}
            }
	}
	
	public DatagramPacket EndReceive(IAsyncResult ar, IPAddress ipe) throws InterruptedException {
		L.l.info("Entering EndReceive.");
		clientEP = new IPAddress(packet.getAddress(), packet.getPort());
		DatagramPacket returnData = packet;
		packet = null;
		L.l.info("Exiting EndReceive.");
		return returnData;
	}
	
	public DatagramPacket receivePacket() throws IOException, InterruptedException {
		byte[] buffer = new byte[4096];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		L.l.info("Waiting to receive packet on port " + Port);
		socket.receive(packet);
		L.l.info("Received packet!");
		return packet;
	}
	
	public void close() {
		try {
			//Extensions.debug("Closing socket", level);
			socket.close();
		}
		catch (Exception e) {
			//already closed
		}
	}
}

