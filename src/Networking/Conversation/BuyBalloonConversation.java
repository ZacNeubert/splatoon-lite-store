/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.BalloonReply;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.RequestMessages.BuyBalloonRequest;
import JavaMessage.RequestMessages.GameListRequest;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.Routing;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import SharedObjects.Penny;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class BuyBalloonConversation extends Conversation {
    public static String key = "BuyBalloonConversation";
    
    public void initialize() {
        retryTimeout = 500;
        state=0;
    }
    
    public BuyBalloonConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(BuyBalloonRequest.class);
            MessageSequence.add(BalloonReply.class);
        }
        try {
            player = MyBalloonStore.ActivePlayer;
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
            //this.routingTo = player.getBalloonStore();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean verifyPenny(Penny p) {
        VerifyPennyConversation vpc = new VerifyPennyConversation();
        vpc.penny = p;
        player.addConversation(vpc, "Verify Penny");
        vpc.start();
        while(!vpc.ConversationFinished) {
            Ext.sleep(100);
        }
        return vpc.isValid;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == BuyBalloonRequest.class) {
            try {
                String message = envelope.getActualMessage();
                BuyBalloonRequest r = (BuyBalloonRequest) Ext.decodeJson(message, MessageClass);
                Penny p = r.Penny;
                //if(p == null || !p.verify()) {
                    
                //}
                this.routingTo = r.ConvId.Pid;
                this.ConvId = r.ConvId;
                state++;
                if(!player.pennyIsUsed(r.Penny.Id)) {
                    if(verifyPenny(p)) {
                        sendEnvelope(MessageSequence.get(state));
                    }
                }
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(BuyBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
