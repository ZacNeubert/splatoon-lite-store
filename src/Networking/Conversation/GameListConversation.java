/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.RequestMessages.GameListRequest;
import JavaMessage.RequestMessages.LoginRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import SharedObjects.GameInfo;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class GameListConversation extends Conversation {
    public static String key = "GameListConversation";
    
    public void initialize() {
        MyBalloonStore.ActivePlayer.setPlayerState(MyBalloonStore.PlayerState.GettingGameList);
        try {
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
        
        /*
        //make a loginrequest
        LoginRequest loginRequest = new LoginRequest(player);
        //send loginrequest
        Envelope loginRequestEnvelope = new Envelope(loginRequest, EndPoint);
        //add loginrequest to history
        history.add(loginRequestEnvelope);
        state++;
        */
    }
    
    public GameListConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(GameListRequest.class);
            MessageSequence.add(GameListReply.class);
        }
        EndPoint = player.RegistryIP;
    }
    
    /*    @Override
    public void resend() {
        try {
            state = 0;
            MessageNumber.ResetSeqNumber();
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(LoginConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == GameListReply.class) {
            GameListReply gameListReply = (GameListReply) Ext.decodeJson(envelope.Message, MessageClass);
            player.GameInfos = Arrays.asList(gameListReply.GameInfo);
            if(player.GameInfos.size() == 0) {
                player.setPlayerState(MyBalloonStore.PlayerState.LoggedIn);
            }
            else {
                player.setPlayerState(MyBalloonStore.PlayerState.HasGameList);
            }
            state++;
        }
    }

}
