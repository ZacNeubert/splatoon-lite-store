/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.RoutedReply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.UmbrellaLoweredNotification;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class LowerUmbrellaConversation extends Conversation {
    public static String key = "LowerUmbrellaConversation";
    public int state = 0;    
    
    public LowerUmbrellaConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(UmbrellaLoweredNotification.class);
            MessageSequence.add(RoutedReply.class);
        }
        retries = Integer.MAX_VALUE;
        player = MyBalloonStore.ActivePlayer;
        try {
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
        } catch (UnknownHostException ex) {
            Logger.getLogger(LowerUmbrellaConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.routingTo = player.GameManagerId;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == UmbrellaLoweredNotification.class) {
            state++;
            try {
                sendEnvelope(MessageSequence.get(state));
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
