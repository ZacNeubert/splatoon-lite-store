/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.RequestMessages.RaiseUmbrellaRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class RaiseUmbrellaConversation extends Conversation {
    public static String key = "RaiseUmbrellaConversation";
    
    public void initialize() {
        try {
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    public RaiseUmbrellaConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(RaiseUmbrellaRequest.class);
            MessageSequence.add(Reply.class);
        }
        try {
            player = MyBalloonStore.ActivePlayer;
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
            player.GameManagerId = player.GameInfos.get(0).GameManagerId;
            this.routingTo = player.GameManagerId;
            //EndPoint = new IPAddress(player.GameInfos.get(0).GameManager.EndPoint);
            //No longer valid with routing
        } catch (UnknownHostException ex) {
            Logger.getLogger(JoinGameConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == Reply.class) {
            String message = envelope.getActualMessage();
            state++;
        }
    }
}
