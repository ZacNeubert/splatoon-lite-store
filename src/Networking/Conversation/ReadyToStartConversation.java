/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.RoutedReply;
import JavaMessage.ReplyMessages.StartGame;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.LoginRequest;
import JavaMessage.RequestMessages.ReadyToStart;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import SharedObjects.GameInfo;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import Networking.IPAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Zac
 */
public class ReadyToStartConversation extends Conversation {
    public static String key = "ReadyToStartConversation";
    public int state = 0;    
    
    public ReadyToStartConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(ReadyToStart.class);
            MessageSequence.add(RoutedReply.class);
            MessageSequence.add(StartGame.class);
        }
        try {
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ReadyToStartConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        retries = Integer.MAX_VALUE;
    }
    
    public int GameId;
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        String actualMessage;
        if(MessageClass == ReadyToStart.class) {
            state++;
            try {
                actualMessage = envelope.getActualMessage();
                ReadyToStart ar = (ReadyToStart) Ext.decodeJson(actualMessage, MessageClass);
                this.ConvId = ar.ConvId.clone();
                GameId = ar.GameId;

                
                for(GameInfo gi : player.GameInfos) {
                    if(gi.GameId == ar.GameId) {
                        this.routingTo = gi.GameManagerId;
                    }
                }
                
                player.setPlayerState(MyBalloonStore.PlayerState.Playing);
                player.ActiveGameId = GameId;
                if(this.routingTo < 0) {
                    throw new Exception("Invalid game");
                }
                sendEnvelope(MessageSequence.get(state));
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ReadyToStartConversation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(MessageClass == StartGame.class) {
            state++;
            try {
                StartGame ar = (StartGame) Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                player.setPlayerState(MyBalloonStore.PlayerState.Playing);
                player.ActiveGameId = GameId;
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
