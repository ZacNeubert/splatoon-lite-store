/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.NextIdReply;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.RequestMessages.NextIdRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import SharedObjects.Balloon;
import static SharedObjects.ProcessInfo.ProcessType.BalloonStore;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class NextIdConversation extends Conversation {
public static String key = "NextIdConversation";
    
    public void initialize() {
        try {
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    //{"__type":"NextIdRequest:#Messages.RequestMessages","ConvId":{},"MsgId":{},"NumberOfIds":390} to 127.0.0.1:12000
    public NextIdConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(NextIdRequest.class);
            MessageSequence.add(NextIdReply.class);
        }
        player = MyBalloonStore.ActivePlayer;
        EndPoint = player.RegistryIP;
        player.setPlayerState(MyBalloonStore.PlayerState.GettingIds);
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == NextIdReply.class) {
            String message = envelope.getActualMessage();
            NextIdReply nextIdReply = (NextIdReply) Ext.decodeJson(message, MessageClass);
            player.setNextId(nextIdReply.NextId);
            player.setIdCount(nextIdReply.NumberOfIds);
            //INFLATE ZE ZEPPELINS
            for(int i=nextIdReply.NextId; i<nextIdReply.NextId+nextIdReply.NumberOfIds; i++) {
                Balloon zeppelin = new Balloon();
                zeppelin.Id = i;
                zeppelin.IsFilled = false;
                zeppelin.sign();
                player.addBalloon(zeppelin);
            }
            player.setPlayerState(MyBalloonStore.PlayerState.HasIds);
            state++;
        }
    }
}

