/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.Bid;
import JavaMessage.ReplyMessages.BidAck;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.RoutedReply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.AuctionAnnouncement;
import JavaMessage.RequestMessages.HitNotification;
import JavaMessage.Routing;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class AuctionAnnouncementConversation extends Conversation {
    public static String key = "AuctionAnnouncementConversation";
    public int state = 0;    
    
    public AuctionAnnouncementConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(AuctionAnnouncement.class);
            MessageSequence.add(Bid.class);
            MessageSequence.add(BidAck.class);
        }
        player = MyBalloonStore.ActivePlayer;
        try {
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
        } catch (UnknownHostException ex) {
            Logger.getLogger(AuctionAnnouncementConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.routingTo = player.getBalloonStore();
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == AuctionAnnouncement.class) {
            state++;
            try {
                AuctionAnnouncement aa = 
                        (AuctionAnnouncement) 
                        Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                Routing rm = 
                        (Routing) 
                        Ext.decodeJson(envelope.Message, Routing.class);
                player.setMinimumBid(aa.MinimumBid);
                this.routingTo = rm.FromProcessId();
                sendEnvelope(MessageSequence.get(state));
            } catch (Exception ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
        else if(MessageClass == BidAck.class) {
            state++;
            //try {
                BidAck ba = 
                    (BidAck) 
                    Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                if(ba.Won) {
                    player.setUmbrella(ba.Umbrella);
                }
                if(ba.Umbrella != null) {
                    player.setUmbrella(ba.Umbrella);
                } 
                /*sendEnvelope(MessageSequence.get(state));
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }*/
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
