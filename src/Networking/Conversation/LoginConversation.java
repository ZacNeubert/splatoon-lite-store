/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.RequestMessages.LoginRequest;
import Networking.EncryptionManager;
import Networking.Envelope;
import dsoak.MyBalloonStore;
import dsoak.MyBalloonStore.PlayerState;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class LoginConversation extends Conversation {
    public static String key = "LoginRequest:#Messages.RequestMessages";
    
    public void initialize() {
        MyBalloonStore.ActivePlayer.setPlayerState(PlayerState.LoggingIn);
        this.state = 0;
        try {
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
        
        /*
        //make a loginrequest
        LoginRequest loginRequest = new LoginRequest(player);
        //send loginrequest
        Envelope loginRequestEnvelope = new Envelope(loginRequest, EndPoint);
        //add loginrequest to history
        history.add(loginRequestEnvelope);
        state++;
        */
    }
    
    public LoginConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(LoginRequest.class);
            MessageSequence.add(LoginReply.class);
        }
        EndPoint = player.RegistryIP;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == LoginReply.class) {
            LoginReply loginReply = (LoginReply) Ext.decodeJson(envelope.Message, MessageClass);
            player.PlayerInfo = loginReply.ProcessInfo;
            player.ProxyJsonEndPoint = loginReply.ProxyEndPoint;
            player.PennyBankJsonEndPoint = loginReply.PennyBankEndPoint;
            MessageNumber.LocalProcessId = player.PlayerInfo.ProcessId;
            
            EncryptionManager.setPennyBankPublicKey(loginReply.PennyBankPublicKey);
            
            player.setPlayerState(PlayerState.LoggedIn);
            state++;
        }
    }
}
