/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.AllowanceDeliveryRequest;
import JavaMessage.RequestMessages.LoginRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class AllowanceDeliveryConversation extends Conversation {
    public static String key = "AllowanceDelivery";
    public int state = 0;    
    
    public AllowanceDeliveryConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(AllowanceDeliveryRequest.class);
            MessageSequence.add(Reply.class);
        }
        EndPoint = player.RegistryIP;
        retries = Integer.MAX_VALUE;
    }
    
    public static List<String> separatePennies(String allPennies) {
        List<String> pstrings = new ArrayList<>();
        boolean started = false;
        int s = 0;
        int i=-1;
        for(char c : allPennies.toCharArray()) {
            i++;
            if(c == '{') {
                s=i;
                started = true;
            }
            if(started && c == '}') {
                started=false;
                String pstring = allPennies.substring(s, i+1);
                pstrings.add(pstring);
            }
        }
        return pstrings;
    }
    
    public void connectAndReceivePennies() throws IOException {
        Socket clientSocket = new Socket(EndPoint.Host, PortForConnection);
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String cashMoney = inFromServer.readLine();
        clientSocket.close();
        List<String> pstrings = separatePennies(cashMoney);
        for(String dubloon : pstrings) {
            try {
                if(dubloon.trim().equals("")) continue;
                player.addPenny(dubloon, 1);
            }
            catch(Exception e) {
                String asdf = e.toString();
            }
        }
    }
    
    int PennyCount = -1;
    int PortForConnection = -1;    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == AllowanceDeliveryRequest.class) {
            state++;
            try {
                AllowanceDeliveryRequest adr = (AllowanceDeliveryRequest) Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                this.ConvId = adr.ConvId.clone();
                sendEnvelope(MessageSequence.get(state));
                
                PortForConnection = adr.PortNumber;
                PennyCount = adr.NumberOfPennies;
                connectAndReceivePennies();
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
