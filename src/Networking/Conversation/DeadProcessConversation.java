/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.DeadProcessNotification;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;

/**
 *
 * @author Zac
 */
public class DeadProcessConversation extends Conversation {
    public static String key = "AliveRequest:#Messages.RequestMessages";
    public int state = 0;    
    
    public DeadProcessConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(DeadProcessNotification.class);
        }
        EndPoint = player.RegistryIP;
        retries = Integer.MAX_VALUE;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == DeadProcessNotification.class) {
            state++;
            int l=0;
            l++;
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }    
}
