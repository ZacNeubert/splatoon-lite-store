/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.LoginRequest;
import JavaMessage.RequestMessages.ShutdownRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class ShutdownConversation extends Conversation {
    public static String key = "ShutdownConversation";
    public int state = 0;    
    
    public ShutdownConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(ShutdownRequest.class);
        }
        EndPoint = player.RegistryIP;
        retries = Integer.MAX_VALUE;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == ShutdownRequest.class) {
            state++;
            try {
                System.exit(0);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
