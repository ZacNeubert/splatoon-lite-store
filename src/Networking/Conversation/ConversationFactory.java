/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.AllowanceDeliveryRequest;
import JavaMessage.RequestMessages.AuctionAnnouncement;
import JavaMessage.RequestMessages.BuyBalloonRequest;
import JavaMessage.RequestMessages.DeadProcessNotification;
import JavaMessage.RequestMessages.ExitGameRequest;
import JavaMessage.RequestMessages.GameStatusNotification;
import JavaMessage.RequestMessages.HitNotification;
import JavaMessage.RequestMessages.ReadyToStart;
import JavaMessage.RequestMessages.ShutdownRequest;
import JavaMessage.RequestMessages.UmbrellaLoweredNotification;
import JavaMessage.RequestMessages.UsedPenniesNotification;
import Networking.Envelope;
import com.google.gson.Gson;
import dsoak.MyBalloonStore;
import java.lang.reflect.InvocationTargetException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Zac
 */
public class ConversationFactory {
    static Map<String, Class> ConversationDictionary;
    
    static Map<Class, Class> MClassToCClass;
    
    public static void initialize(MyBalloonStore player) {
        ConversationDictionary = new HashMap<>();
        ConversationDictionary.put(LoginConversation.key, LoginConversation.class);
        ConversationDictionary.put(GameListConversation.key, GameListConversation.class);
        ConversationDictionary.put(JoinGameConversation.key, JoinGameConversation.class);
        ConversationDictionary.put(LogoutConversation.key, LogoutConversation.class);
        ConversationDictionary.put(ReadyToStartConversation.key, ReadyToStartConversation.class);
        ConversationDictionary.put(HitNotificationConversation.key, HitNotificationConversation.class);
        ConversationDictionary.put(ExitGameConversation.key, ExitGameConversation.class);
        ConversationDictionary.put(BuyBalloonConversation.key, BuyBalloonConversation.class);
        ConversationDictionary.put(ShutdownConversation.key, ShutdownConversation.class);
        ConversationDictionary.put(RaiseUmbrellaConversation.key, RaiseUmbrellaConversation.class);
        ConversationDictionary.put(NextIdConversation.key, NextIdConversation.class);
        ConversationDictionary.put(LeaveGameConversation.key, LeaveGameConversation.class);
        Conversation.player = player;
        
        MClassToCClass = new HashMap<>();
        MClassToCClass.put(AliveRequest.class, AliveConversation.class);
        MClassToCClass.put(AllowanceDeliveryRequest.class, AllowanceDeliveryConversation.class);
        MClassToCClass.put(ReadyToStart.class, ReadyToStartConversation.class);
        MClassToCClass.put(GameStatusNotification.class, GameStatusConversation.class);
        MClassToCClass.put(HitNotification.class, HitNotificationConversation.class);
        MClassToCClass.put(ExitGameRequest.class, ExitGameConversation.class);   
        MClassToCClass.put(ShutdownRequest.class, ShutdownConversation.class);
        MClassToCClass.put(AuctionAnnouncement.class, AuctionAnnouncementConversation.class);
        MClassToCClass.put(UmbrellaLoweredNotification.class, LowerUmbrellaConversation.class);
        MClassToCClass.put(DeadProcessNotification.class, DeadProcessConversation.class);
        MClassToCClass.put(BuyBalloonRequest.class, BuyBalloonConversation.class);
        MClassToCClass.put(UsedPenniesNotification.class, UsedPenniesConversation.class);
    }
    
    public static Conversation createIncomingConversation(Class key, Envelope envelope) 
            throws NoSuchMethodException, 
                   InstantiationException, 
                   IllegalAccessException, 
                   IllegalArgumentException, 
                   InvocationTargetException 
    {
        //Conversation AliveConversation = new AliveConversation();
        //AliveConversation.IncomingQueue.add(envelope);
        //return AliveConversation;
        Class ConversationClass = MClassToCClass.get(key);
        MyBalloonStore player = MyBalloonStore.ActivePlayer;
        try {
            Conversation conversation = 
                    (Conversation) 
                    ConversationClass
                    .getDeclaredConstructor()
                    .newInstance();
            conversation.IncomingQueue.add(envelope);
            return conversation;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        //something went wrong
        return null;
    }
    
    public static Conversation create(String key) 
            throws NoSuchMethodException, 
                   InstantiationException, 
                   IllegalAccessException, 
                   IllegalArgumentException, 
                   InvocationTargetException 
    {
        Class ConversationClass = ConversationDictionary.get(key);
        try {
            Conversation conversation = 
                    (Conversation) 
                    ConversationClass
                    .getDeclaredConstructor()
                    .newInstance();
            return conversation;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        //something went wrong
        return null;
    }
}
