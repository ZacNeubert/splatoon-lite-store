/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.RequestMessages.PennyValidation;
import JavaMessage.Routing;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import Networking.UdpClient;
import SharedObjects.Penny;
import dsoak.MyBalloonStore;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class VerifyPennyConversation extends Conversation {
    public static String key = "JoinGameConversation";
    
    public void initialize() {
        MyBalloonStore.ActivePlayer.setPlayerState(MyBalloonStore.PlayerState.JoiningGame);
        try {
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    public VerifyPennyConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(PennyValidation.class);
            MessageSequence.add(Reply.class);
        }
        try {
            player = MyBalloonStore.ActivePlayer;
            EndPoint = new IPAddress(player.PennyBankJsonEndPoint);
        } catch (UnknownHostException ex) {
            Logger.getLogger(JoinGameConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Penny penny;
    @Override
    public void sendEnvelope(Class c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InterruptedException {
        Message message = (Message) c.getConstructor(MyBalloonStore.class).newInstance(player);
        Method m = c.getMethod("isRouted");
        Boolean isRouted = (Boolean) m.invoke(null, null);
        
        if(c==PennyValidation.class) {
            PennyValidation pv = (PennyValidation) message;
            pv.Pennies = new Penny[] {penny};
        }

        if(state == 0 && this.ConvId == null) {
            this.ConvId = MessageNumber.Create();
            message.MsgId = this.ConvId;
        }
        else {
            message.MsgId = MessageNumber.Create();
        }
        message.ConvId = this.ConvId;
        if(isRouted) {
            message = new Routing(message, routingTo); //probably
        }
        Envelope envelope = new Envelope(message, EndPoint);
        previousMessage = envelope;
        //player.guiMessages.add("Sending " + envelope.Message + " to " + envelope.Endpoint);
        UdpClient.udpClient.SendEnvelope(envelope);
        history.add(envelope);
        state++;
    }
    
    public boolean isValid = false;
    public boolean ConversationFinished = false;
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == Reply.class) {
            String message = envelope.getActualMessage();
            Reply reply = (Reply) Ext.decodeJson(message, MessageClass);
            if(reply.Success) {
                isValid=true;
            }
            ConversationFinished = true;
            state++;
        }
    }
}
