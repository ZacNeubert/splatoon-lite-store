/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.ReplyMessages.BalloonReply;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.NextIdReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.StartGame;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.AllowanceDeliveryRequest;
import JavaMessage.RequestMessages.AuctionAnnouncement;
import JavaMessage.RequestMessages.BuyBalloonRequest;
import JavaMessage.RequestMessages.DeadProcessNotification;
import JavaMessage.RequestMessages.ExitGameRequest;
import JavaMessage.RequestMessages.GameStatusNotification;
import JavaMessage.RequestMessages.HitNotification;
import JavaMessage.RequestMessages.LowerUmbrella;
import JavaMessage.RequestMessages.ReadyToStart;
import JavaMessage.RequestMessages.ShutdownRequest;
import JavaMessage.RequestMessages.UsedPenniesNotification;
import Networking.Conversation.Conversation;
import Networking.Conversation.ConversationFactory;
import Networking.Conversation.AliveConversation;
import Networking.Conversation.DeadProcessConversation;
import Networking.Conversation.LowerUmbrellaConversation;
import Networking.Conversation.NextIdConversation;
import dsoak.MyBalloonStore;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class Dispatcher {
    static Map<String, Class> messageMap;
    static
    {
        initDictionary();
    }
    
    static void initForTests() { //ONLY FOR TESTS
        initDictionary();
    }
    
    private static void initDictionary() {
        messageMap = new HashMap<String, Class>();
        messageMap.put("AliveRequest", AliveRequest.class);
        messageMap.put("LoginReply", LoginReply.class);
        messageMap.put("GameListReply", GameListReply.class);
        messageMap.put("JoinGameReply", JoinGameReply.class);
        messageMap.put("AllowanceDelivery", AllowanceDeliveryRequest.class);
        //messageMap.put("Reply", LoginReply.class);
        messageMap.put("ReadyToStart", ReadyToStart.class);
        messageMap.put("StartGame", StartGame.class);
        messageMap.put("GameStatusNotification",GameStatusNotification.class);
        messageMap.put("HitNotification", HitNotification.class);
        messageMap.put("ExitGameRequest", ExitGameRequest.class);
        messageMap.put("BuyBalloonRequest", BuyBalloonRequest.class);
        messageMap.put("ShutdownRequest", ShutdownRequest.class);
        messageMap.put("AuctionAnnouncement", AuctionAnnouncement.class);
        messageMap.put("UmbrellaLowered", LowerUmbrella.class);
        messageMap.put("DeadProcessNotification", DeadProcessNotification.class);
        messageMap.put("NextIdReply", NextIdReply.class);
        messageMap.put("UsedPenniesNotification", UsedPenniesNotification.class);
        messageMap.put("\"Reply:#Messages.ReplyMessages", Reply.class);
    }
    
    static void Dispatch(Envelope envelope) {
        Conversation targetConv = getConversation(envelope);
        if(targetConv == null) {
            try {
                MyBalloonStore player = MyBalloonStore.ActivePlayer;
                String key = getApplicableKey(envelope);
                Class FirstMessageOfConvClass = messageMap.get(key);
                Conversation conversation = ConversationFactory.createIncomingConversation(FirstMessageOfConvClass,envelope);
                if(conversation == null) {
                    return;
                }
                player.addConversation(conversation, "Dispatcher (incoming Message)");
                conversation.start();
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception otherException) {
                Logger.getLogger(Dispatcher.class.getName()).log(Level.SEVERE, null, otherException);
            }
        }
        else {
            targetConv.IncomingQueue.add(envelope);
        }
    }
    
    private static String getApplicableKey(Envelope e) {
        String text = e.Message;
        for(String indicator : messageMap.keySet()) {
            if(text.contains(indicator)) {
                return indicator;
            }
        }
        return null;
    }
    
    private static Conversation getConversation(Envelope e) {
        MyBalloonStore player = MyBalloonStore.ActivePlayer;
        String text = e.Message;
        
        L.debug("Received " + text);
        Class MessageClass = null;
        for(String indicator : messageMap.keySet()) {
            if(text.contains(indicator)) {
                MessageClass = messageMap.get(indicator);
            }
        }
        Message message = (Message) Ext.decodeJson(text, MessageClass);
        
        List<Conversation> allConvs = new ArrayList<Conversation>();
        allConvs.addAll(player.Conversations);
        for(Conversation c : allConvs) {
            if(c != null && c.ConvId != null && c.ConvId.equals(message.ConvId)) {
                return c;
            }
        }
        return null;
    }
}
