/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Zac
 */
public class Hasher {
    static MessageDigest messageDigest = null;
    
    public static int[] getHash(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if(messageDigest == null) messageDigest=MessageDigest.getInstance("SHA-1");
        byte[] databytes = data.getBytes("UTF8");
        byte[] spacedDataBytes = new byte[databytes.length*2];
        for(int i=0; i<databytes.length; i++) {
            spacedDataBytes[i*2] = databytes[i];
            spacedDataBytes[i*2+1] = (byte) 0;
        }
        int[] unsignedHash = getHash(spacedDataBytes);
        return unsignedHash;
    }
    
    public static int[] getHash(byte[] data) throws NoSuchAlgorithmException {
        if(messageDigest == null) messageDigest=MessageDigest.getInstance("SHA-1");
        byte[] hash = messageDigest.digest(data);
        int[] unsignedHash = Ext.getUnsignedInts(hash);
        return unsignedHash;
    }
}
