/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import SharedObjects.IntPublicKey;
import SharedObjects.PublicKey;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;

/**
 *
 * @author Zac
 */
public class EncryptionManager {
    // symmetric algorithm for data encryption
    final static String ALGORITHM = "RSA";
    // Padding for symmetric algorithm
    final static String PADDING_MODE = "/CBC/PKCS5Padding";
    // character encoding
    final static String CHAR_ENCODING = "UTF-8";
    // provider for the crypto
    final static String CRYPTO_PROVIDER = "Entrust";
    // RSA algorithm used to encrypt symmetric key
    final static String RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";
    // symmetric key size (128, 192, 256) if using 192+ you must have the Java
    // Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files
    // installed
    //final static int KEY_SIZE = 512;
    
    public static IntPublicKey getPublicKey() throws Exception {
        if(rsaPublicKey == null) {
            try {
                generateKey();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchPaddingException ex) {
                Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        IntPublicKey publicKey = new IntPublicKey();
        byte[] signedExponent = rsaPublicKey.getPublicExponent().toByteArray();
        byte[] signedModulus = rsaPublicKey.getModulus().toByteArray();
        publicKey.Exponent = Ext.getUnsignedInts(signedExponent);
        publicKey.Modulus = Ext.getUnsignedInts(signedModulus);
        
        return publicKey;
    }
    
    /*public static int[] getPrivateKey() {
        if(privateKey == null) {
            try {
                generateKey();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        int[] key = Ext.getUnsignedInts(privateKey);
        return key;
    }*/
    
    private static RSAPublicKey rsaPublicKey = null;
    private static RSAPrivateKey rsaPrivateKey = null;
    
    public static RSAPrivateKey getPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, Exception {
        if(rsaPrivateKey == null) generateKey();
        return rsaPrivateKey;
    }
    
    public static void generateKey() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, Exception {
        //Base64.getDecoder().decode("applesauce");
        //KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        //KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        /*BigInteger pubExp = new BigInteger("10001", 16);
        BigInteger modulus = new BigInteger("98F77A5EA3A0F1C8943F45F0A23F0A8685F21DEDE0CBF0F431168BD130F5F8A5EE0D0773D7FC1E4C3C0838989292EBDF8166225F16BB4C57E831FEB73B9625574F7DB7AC893F7EE23C933B8CBE798C8AD1145A3193832EA90B3A15EA3843E31BC5E2B4C79A9C5FEA86D40D462070F887D7B1652F47AE4B75F21373224983912D", 16);
        RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(modulus, pubExp);
        rsaPublicKey = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);
        rsaPrivateKey = (RSAPrivateKey) PrivateKeyReader.get();*/
        
        
        //Generating new keys
        KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
        keygen.initialize(512);
        KeyPair keyPair = keygen.genKeyPair();
        rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
    }
    
    /*static final String filestr = "C:\\Users\\Zac\\Documents\\NetBeansProjects\\balloon-store\\private_key.der";
    public static class PrivateKeyReader {
        public static PrivateKey get() throws Exception {
            //get the private key
            File file = new File(filestr);
            FileInputStream fis = new FileInputStream(file);
            DataInputStream dis = new DataInputStream(fis);

            byte[] keyBytes = new byte[(int) file.length()];
            dis.readFully(keyBytes);
            dis.close();

          PKCS8EncodedKeySpec spec =
            new PKCS8EncodedKeySpec(keyBytes);
          KeyFactory kf = KeyFactory.getInstance("RSA");
          return kf.generatePrivate(spec);
        }
    }*/
    
    
    /*public static byte[] getExponent() {
        byte[] exponent = new byte[3];
        exponent[0] = (byte) 0x01;
        exponent[1] = (byte) 0x00;
        exponent[2] = (byte) 0x01;
        return exponent;
    }*/

    public static RSAPublicKey pennyBankPublicKey = null;
    public static void setPennyBankPublicKey(PublicKey publicKey) {
        try {
            BigInteger exponentInteger = new BigInteger(publicKey.Exponent);
            BigInteger modulusInteger = new BigInteger(publicKey.Modulus);
            RSAKeyParameters params = new RSAKeyParameters(false, modulusInteger, exponentInteger);
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulusInteger, exponentInteger);
            Security.addProvider(new BouncyCastleProvider());
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            pennyBankPublicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(EncryptionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
