/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author zacneubert
 */
public class IPAddress {
    public InetAddress Host;
    public int Port;
    
    public IPAddress(InetAddress inetaddress, int port) {
        Host = inetaddress;
        Port = port;
    }
    
    public IPAddress(JsonIPEndPoint jiep) throws UnknownHostException {
        Host = InetAddress.getByName(jiep.Host);
        Port = jiep.Port;
    }
    
    @Override
    public String toString() {
        return new JsonIPEndPoint(this).toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        IPAddress other = (IPAddress) obj;
        JsonIPEndPoint otherep = new JsonIPEndPoint(other);
        JsonIPEndPoint thisep = new JsonIPEndPoint(this);
        return thisep.equals(otherep);
    }
}
